# GS_CARDDEALER
## a helper for the TTRPG Gun&Slinger, by nevynn {find their last name}

This project is intended to allow for remote play with the mechanics of the game. 
As the pandemic is still a factor in many people's lives, and the G&S mechanics seem ill-suited to traditional dice and card bots, I hope to build something specifically for the task.

This is planned to be a CLI app written in rust firt, with a GUI hopefully added some time in the future.
