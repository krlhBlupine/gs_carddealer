fn main() {
    let deck0 = Deck::new(52);
    deck0.shuffle;

    let gun = Hand::new(5, deck0);
    let slinger = Hand::new(5, deck0);
    let guide = Hand::new_guide(5, deck0);
}

enum Card {
    hearts(u8),
    clubs(u8),
    diamonds(u8),
    spades(u8),
    joker(bool),
}

struct Hand {
    size: u8,
    cards_held: , // vector here?
}

struct Deck {
    size: u32,
    cards_order: , // vector here too?
    discarded: , // ^^
}
